# 羊了个羊-多线程通关版



<div align=center>
<img src="https://img.shields.io/badge/sheep-1.0-green" width="XXX" height="XXX" />
<img src="https://img.shields.io/badge/springboot-2.2-yellowgreen.svg" width="XXX" height="XXX" />
<img src="https://img.shields.io/github/license/pig-mesh/pig" width="XXX" height="XXX" />
</div>

#### 声明

仅供技术学习和娱乐,请勿用于商业用途

原作者 : https://blog.csdn.net/qq_49619863/article/details/126903836



#### 物料

抓包工具 : HTTP Debugger Pro 

附下载地址 : https://cloud.189.cn/t/JvYJNf3ARJvu (访问码:2rwt)

**pc端微信**



#### 操作步骤

1、打开HTTP Debugger Pro
2、打开pc端微信
3、打开小程序
观察HTTP Debugger Pro中的请求，找到目标请求，点击打开复制出t的值即可

![](https://solink.xyz/89d2ca7f30aa4324b2b15bcf1171965d.jpeg)



#### 运行结果



![](https://solink.xyz/20220918014456.png)


#### 成果展示

<div align=left><img src="https://solink.xyz/20220918185850.jpg" width="30%" height="30%"></div>



------

[^小程序]: 搜索 : 咔哒KADA
[^个人博客]: https://www.solink.xyz



