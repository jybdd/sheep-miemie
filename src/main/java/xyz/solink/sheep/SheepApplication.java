package xyz.solink.sheep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName sheep-miemie - SheepApplication
 * @Description [此类用于: ]
 * @Author liming-jybdd@foxmail.com
 * @Date 2022/9/17 17:53
 * @Version 1.0
 **/
@SpringBootApplication
public class SheepApplication {

    public static void main(String[] args) {
        SpringApplication.run(SheepApplication.class, args);
    }
}