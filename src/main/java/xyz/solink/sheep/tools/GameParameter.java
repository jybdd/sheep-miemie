package xyz.solink.sheep.tools;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @ClassName sheep-miemie - Sheep
 * @Description [此类用于: ]
 * @Author liming-jybdd@foxmail.com
 * @Date 2022/9/17 18:02
 * @Version 1.0
 **/

@Data
@Accessors(chain = true)
public class GameParameter implements Serializable {
    private Integer rank_score;
    private Integer rank_state;
    private Integer rank_time;
    private Integer rank_role;
    private Integer skin;
}