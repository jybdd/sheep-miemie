package xyz.solink.sheep.tools;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

/**
 * @ClassName sheep-miemie - SheepTools
 * @Description [此类用于: ]
 * @Author liming-jybdd@foxmail.com
 * @Date 2022/9/17 17:56
 * @Version 1.0
 **/

public class SheepTools {
    /**
     * 游戏完成接口相关
     */
    public static final String GAME_COMPLETE_INTERFACE = "https://cat-match.easygame2021.com/sheep/v1/game/game_over?%s";
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 MicroMessenger/7.0.4.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat/WMPF";
    /**
     * t  抓包获取
     */
    public static final String TOKEN = "XXX";

    private static <T, R> R getRequestParameter(T obj, Function<T, R> function) {
        return function.apply(obj);
    }

    /**
     * 构造请求参数
     *
     * @return
     */
    private static String buildParameter() {
        int time = RandomUtil.randomInt(1, 2);
        GameParameter gameParameter = new GameParameter().setRank_score(1)
                .setRank_state(1).setRank_time(time).setRank_role(1).setSkin(1);
        String requestParameter = getRequestParameter(gameParameter, (g) -> {
            StringBuilder builder = new StringBuilder();
            Field[] fields = g.getClass().getDeclaredFields();
            for (Field field : fields) {
                try {
                    field.setAccessible(true);
                    builder.append(field.getName().concat("=")
                            .concat(String.valueOf(field.get(g))).concat("&"));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return builder.substring(0, builder.length() - 1);
        });
        return requestParameter;
    }

    /**
     * 执行
     *
     * @param requestParameter
     */
    private static void execute(String requestParameter, AtomicInteger atomicInteger) {
        String body = null;
        body = HttpUtil.createGet(String.format(GAME_COMPLETE_INTERFACE, requestParameter))
                .header("Host", "cat-match.easygame2021.com")
                .header("user-agent", USER_AGENT)
                .header("t", TOKEN).execute().body();
        JSONObject parseObject = JSON.parseObject(body);
        if (Objects.equals(parseObject.get("err_code"), 0)) {
            System.out.println(Thread.currentThread().getName() + "=>完成第" + atomicInteger.get() + "次通关");
        }
    }

    public static void main(String[] args) {
        String requestParameter = buildParameter();
        System.out.println("*******【羊了个羊】*******");
        System.out.print("请输入需要通关次数：");
        Scanner scanner = new Scanner(System.in);
        int gameNum = scanner.nextInt();
        System.out.print("请输入需要线程数：");
        int threadNum = scanner.nextInt();
        AtomicInteger initialTimes = new AtomicInteger(1);
        for (int i = 1; i <= threadNum; i++) {
            new Thread(() -> {
                while (initialTimes.get() <= gameNum) {
                    try {
                        System.out.println(Thread.currentThread().getName() + "=>开始第" + initialTimes.getAndIncrement() + "次通关");
                        execute(requestParameter, initialTimes);
                    } catch (Exception e) {
                        System.out.println(Thread.currentThread().getName() + "=>服务的超时，请稍后再试.....");
                    }
                }
                System.out.println("*******【结束】*******");
            }, "sheep线程--" + i).start();
        }
    }

}
